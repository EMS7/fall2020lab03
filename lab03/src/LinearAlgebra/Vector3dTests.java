//Estefan Maheux-Saban 1931517

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void test() {
		
		//verifies the getters
		Vector3d v1 = new Vector3d(2,4,6);
		Vector3d v2 = new Vector3d(3,5,7);
		Vector3d exp_vector = new Vector3d(5,9,13);
		assertEquals(2, v1.getAxisX());
		assertEquals(4, v1.getAxisY());
		assertEquals(6, v1.getAxisZ());
		//verifies magnitude method
		assertEquals(7.483314773547883, v1.magnitude(), 2);
		//verifies dot product method
		assertEquals(68, v1.dotProduct(v2));
		//verifies add method
		assertTrue(exp_vector.CompareAxis(v1.add(v2)));
		
		
		
		
	}

}
