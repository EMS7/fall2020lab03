//Estefan Maheux-Saban 1931517

package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
	private double axisX;
	private double axisY;
	private double axisZ;
	
	
	//constructors
	public Vector3d() {
		
	}
	
	public Vector3d(double x, double y, double z) {
		this.axisX = x;
		this.axisY = y;
		this.axisZ = z;		
	}
	
	//getters
	public double getAxisX() {
		return this.axisX;
	}
	
	public double getAxisY() {
		return this.axisY;
	}
	
	public double getAxisZ() {
		return this.axisZ;
	}
	
	
	//magnitude method
	
	public double magnitude() {
		return Math.sqrt(Math.pow(2, this.axisX) + Math.pow(2, this.axisY) + Math.pow(2, this.axisZ));
	}
	
	//dot product method
	public double dotProduct(Vector3d v) {
		return (this.axisX * v.axisX) + (this.axisY * v.axisY) + (this.axisZ * v.axisZ);
	}
	
	//add method
	public Vector3d add(Vector3d v) {
		return new Vector3d((this.axisX + v.axisX),(this.axisY + v.axisY),(this.axisZ + v.axisZ));
		
	}
	
	//Comparator
	public boolean CompareAxis(Vector3d v) {
			return (this.axisX == v.axisX && this.axisY == v.axisY && this.axisZ == v.axisZ);
	}
	
	

}
